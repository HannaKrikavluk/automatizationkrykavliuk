import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.security.Key;
import java.util.List;

public class Main {
    WebDriver driver;

    @BeforeClass
    public static void setup() {
    System.setProperty("webdriver.gecko.driver","C:/Users/Yamert/Downloads/geckodriver-v0.26.0-win64/geckodriver.exe");
    }

    @Before
    public void precondition() {
        driver = new FirefoxDriver();

    }

    @After
    public void postCondition()  {

    }

    @Test

        public void runTest() throws InterruptedException {


            User user = CreateRandomizedUser();
            if(registrationUserData(user.email, user.password, user.firstName, user.lastName, user.workPhone, user.mobilePhone, user.gender, user.position)) {
                driver.navigate().refresh();
                authorizationUserData(user.email, user.password);
                if (findYourselfInEmployees(user.mobilePhone, user.firstName,user.lastName))
                {
                    System.out.println("first and last names matches");
                }
        }
           }


    public User CreateRandomizedUser()
    {
        User user = new User();
        String[] codes = new String[]{"50","63","66","67","68","91","92","93","97","96"};
        String randomPhoneNumber = "380" + codes[randomInteger(0,codes.length - 1)] + getRandom(7);
        String randomEmail = makeEmail();
        user.mobilePhone = randomPhoneNumber;
        user.email = randomEmail;
        return  user;
    }




    public boolean registrationUserData(String email, String password, String firstName, String lastName, String workPhone, String mobilePhone, String gender, String position)
    {
        driver.get("https://user-data.hillel.it/html/registration.html");
        driver.findElement(By.cssSelector(".registration")).sendKeys(Keys.ENTER);
        driver.findElement(By.cssSelector("#first_name")).sendKeys(firstName);
        driver.findElement(By.cssSelector("#last_name")).sendKeys(lastName);
        driver.findElement(By.cssSelector("#field_work_phone")).sendKeys(workPhone);
        driver.findElement(By.cssSelector("#field_phone")).sendKeys(mobilePhone);

        driver.findElement(By.cssSelector("#field_email")).sendKeys(email);
        driver.findElement(By.cssSelector("#field_password")).sendKeys(password);
        driver.findElement(By.cssSelector(gender)).click();
        Select dropdown = new Select(driver.findElement(By.id("position")));
        dropdown.selectByValue(position);
        driver.findElement(By.cssSelector("#button_account")).sendKeys(Keys.ENTER);

        WebDriverWait wait = new WebDriverWait(driver, 3);

        try {
            wait.until(ExpectedConditions.alertIsPresent());
            Alert alert = driver.switchTo().alert();
            String message = alert.getText();
            if(!message.equals("Successful registration"))
            {
                System.out.println("Registration failed: " + message);
                alert.accept();
                return false;
            }
            alert.accept();
            return true;
        } catch (TimeoutException eTO) {
            System.out.println(eTO);
            return false;
        }

    }

    public void authorizationUserData(String email, String password)
    {

        driver.findElement(By.cssSelector(".authorization")).click();
        driver.findElement(By.cssSelector("#email")).sendKeys(email);
        driver.findElement(By.cssSelector("#password")).sendKeys(password);
        driver.findElement(By.cssSelector(".login_button")).sendKeys(Keys.ENTER);
    }

    public boolean findYourselfInEmployees(String mobilePhone, String firstName, String lastName) throws InterruptedException {
        Thread.sleep(2000);
        driver.findElement(By.cssSelector("#employees")).sendKeys(Keys.ENTER);
        driver.findElement(By.cssSelector("#mobile_phone")).sendKeys(mobilePhone);
        Thread.sleep(2000);
        driver.findElement(By.cssSelector("#search")).click();
        Thread.sleep(2000);
        List<WebElement> trs = driver.findElements(By.cssSelector("#table > tr"));

        boolean itsMe = false;
        if(trs.size() > 0)
        {
            List<WebElement> tds = trs.get(0).findElements(By.cssSelector("tr > td"));
            itsMe = tds.get(0).getText().equals(firstName) && tds.get(1).getText().equals(lastName);
        }

        return  itsMe;

    }





    public String makeEmail() {
    char[] bodySymbols = "abcdefghijklmnopqrstuvwxyz_0123456789".toCharArray();
    char[] tailSymbols = "abcdefghijklmnopqrstuvwxyz".toCharArray();

        String body = "";
        String tail = "";
        String domain = "";

        for (int i = 0; i < randomInteger(3, 15); i++)
        {
            body += bodySymbols[randomInteger(0, bodySymbols.length - 1)];
        }

        for (int i = 0; i < randomInteger(3, 10); i++)
        {
            tail += tailSymbols[randomInteger(0, tailSymbols.length - 1)];
        }

        for (int i = 0; i < randomInteger(2, 6); i++)
        {
            domain += tailSymbols[randomInteger(0, tailSymbols.length - 1)];
        }

        String strEmail = body + "@" + tail + "." + domain;
        return strEmail;
    }

    public int randomInteger(int min, int max) {
        return (int)Math.floor(Math.random() * (max - min + 1)) + min;
    }

    public  int getRandom(int length)
    {
        return (int)Math.floor(Math.pow(10, length-1) + Math.random() * 9 * Math.pow(10, length-1));
    }

}
